import * as express from 'express';
import {plainToClass} from 'class-transformer';
import {validate, ValidationError} from 'class-validator';
import * as HttpStatus from 'http-status-codes'

function validationMiddleware<T>(type: any, reqType: string, skipMissingProperties: boolean): express.RequestHandler {
    return (req, res, next) => {
        validate(plainToClass(type, req[reqType]), {skipMissingProperties})
            .then((errors: ValidationError[]) => {
                if (errors.length > 0) {
                    const messageArray = {};
                    errors.forEach((error: ValidationError) => {
                        const key = Object.keys(error.constraints)[0];
                        messageArray[error.property] = error.constraints[key]
                    });
                    res.status(HttpStatus.BAD_REQUEST).send(messageArray)
                } else {
                    next();
                }
            });
    };
}

export function validateBody(type, skipMissingProperties = false) {
    return validationMiddleware(type, 'body', skipMissingProperties)
}

export function validateQuery(type, skipMissingProperties = false) {
    return validationMiddleware(type, 'query', skipMissingProperties)
}
