import {NextFunction, Request, Response, Router} from 'express';
import MovieService from './movie.service';
import * as HttpStatus from 'http-status-codes'
import MovieDto from './dto/movie.dto';
import {validateBody, validateQuery} from '../../middlewares/validation.middleware';
import MovieQueryDto from './dto/movie-query.dto';
import Controller from '../../interfaces/controller.interface';

export default class MovieController implements Controller {

    path = 'movie';
    router: Router = Router();

    constructor(
        private movieService: MovieService
    ) {
        this.router
            .post('/', validateBody(MovieDto), this.addMovie)
            .get('/', validateQuery(MovieQueryDto), this.getMovies);
    }

    private addMovie = (req: Request, res: Response, next: NextFunction) => {
        const movie: MovieDto = req.body;

        try {
            const createdMovie = this.movieService.addMovie(movie);
            res.status(HttpStatus.CREATED).send(createdMovie);
        } catch (e) {
            next(e)
        }
    };

    private getMovies = (req: Request, res: Response, next: NextFunction) => {
        const query: MovieQueryDto = req.query as MovieQueryDto;
        try {
            const movies = this.movieService.getMovies(query);
            res.status(HttpStatus.OK).send(movies);
        } catch (e) {
            next(e)
        }
    }
}
