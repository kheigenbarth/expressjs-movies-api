import {asClass, createContainer, InjectionMode} from 'awilix';
import MovieController from './movie.controller';
import MovieService from './movie.service';
import MovieDb from './movie.db';

const moduleContainer = createContainer({
    injectionMode: InjectionMode.CLASSIC,
}).register({
    movieService: asClass(MovieService),
    controller: asClass(MovieController).classic(),
    db: asClass(MovieDb).singleton()
});

const MovieModule = moduleContainer.cradle.controller;

export default MovieModule

