import MovieDb from './movie.db';
import MovieQueryDto from './dto/movie-query.dto';
import MovieDto from './dto/movie.dto';
import Movie from './interfaces/movie.interface';

export default class MovieService {

    constructor(
        private db: MovieDb
    ) {
        this.db = db;
    }

    addMovie(movie: MovieDto): Movie {
        return this.db.addMovie(movie);
    }

    getMovies(query: MovieQueryDto): Movie[] | Movie {
        const {genres, duration} = query;
        let movies = this.db.getMovies();

        if (genres) {
            movies = movies.filter((movie: Movie) =>
                movie.genres.some(genre => genres.includes(genre) &&
                    (Math.abs(movie.runtime - (duration || movie.runtime)) <= 10)));

            const countDemandedGenres = (movie: Movie) =>
                movie.genres.reduce((count, currentGenre) =>
                    genres.includes(currentGenre) ? count + 1 : count, 0);

            return movies.sort((a: Movie, b: Movie) => countDemandedGenres(b) - countDemandedGenres(a));
        }

        if(duration) {
            movies = movies.filter((movie: Movie) => Math.abs(movie.runtime - duration) <= 10);
        }

        return movies[Math.floor(Math.random() * movies.length)];
    }
}
