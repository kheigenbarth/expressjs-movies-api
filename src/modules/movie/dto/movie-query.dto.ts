import {IsArray, IsIn, IsInt, IsOptional, Min} from 'class-validator';
import {MovieGenres} from '../interfaces/movie.interface';
import {Type} from 'class-transformer';

export default class MovieQueryDto {

    @IsOptional()
    @IsArray()
    @IsIn(MovieGenres, {each: true})
    @Type(() => Array)
    genres?: string[];

    @IsOptional()
    @IsInt()
    @Min(0)
    @Type(() => Number)
    duration?: number;
}
