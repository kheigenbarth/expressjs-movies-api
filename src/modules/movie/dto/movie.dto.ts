import {IsNumber, IsOptional, IsString, IsArray, IsIn} from 'class-validator';
import {MovieGenres} from '../interfaces/movie.interface';

export default class MovieDto {

    @IsOptional()
    id!: number;

    @IsString()
    title!: string;

    @IsArray()
    @IsIn(MovieGenres, {each: true})
    genres!: Array<string>;

    @IsNumber()
    year!: number;

    @IsNumber()
    runtime!: number;

    @IsOptional()
    @IsString()
    director!: string;

    @IsOptional()
    @IsString()
    actors!: string;

    @IsOptional()
    @IsString()
    plot!: string;

    @IsOptional()
    @IsString()
    posterUrl!: string;
}
