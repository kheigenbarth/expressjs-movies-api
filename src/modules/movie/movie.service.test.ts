import MovieService from './movie.service';
import MovieDb from './movie.db';
import Movie from "./interfaces/movie.interface";

const movieService = new MovieService(new MovieDb());

test('Test movie query (genres and duration)', () => {

    let expectedGenres, expectedDuration, expectedLen, expectedMovieTitles;

    expectedGenres = ['Comedy', 'Drama', 'Action'];
    expectedDuration = 70;
    expectedLen = 3;
    expectedMovieTitles = ['Corpse Bride', 'Before Sunset', 'Shogun'];

    const movies = <Array<Movie>> movieService.getMovies({genres: expectedGenres, duration: expectedDuration});
    const correctMoviesFlag = movies.every((m: Movie) => expectedMovieTitles.includes(m.title));

    expect(movies).toHaveLength(expectedLen);
    expect(correctMoviesFlag).toBe(true);
});

test('Test movie query (only genres)', () => {

    let expectedGenres, expectedLen, expectedMovieTitles;

    expectedGenres = ['Western', 'Film-Noir'];
    expectedLen = 3;
    expectedMovieTitles = ['The Third Man', 'Django Unchained', 'Sunset Boulevard'];

    const movies = <Array<Movie>> movieService.getMovies({genres: expectedGenres});
    const correctMoviesFlag = movies.every((m: Movie) => expectedMovieTitles.includes(m.title));

    expect(movies).toHaveLength(expectedLen);
    expect(correctMoviesFlag).toBe(true);
});

