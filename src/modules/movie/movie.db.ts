import Movie from './interfaces/movie.interface';
import * as path from 'path';
import * as fs from 'fs';

export default class MovieDb {

    readonly movies: Movie[];
    readonly genres: string[];
    private lastIndex: number;
    readonly filename: string = 'db.json';
    readonly dbPath: string = path.join(__dirname, '../../../data', this.filename);

    constructor() {
        const {movies, genres} = this.loadFile();
        const {id}: Movie = movies[movies.length - 1];

        this.movies = movies;
        this.genres = genres;
        this.lastIndex = id;
    }

    private async saveFile() {
        const writeStream = fs.createWriteStream(this.dbPath);
        const content = JSON.stringify({genres: this.genres, movies: this.movies});
        writeStream.write(content, 'utf8');
        writeStream.end();
    }

    private loadFile(): {movies: Array<Movie>, genres: Array<string> } {
        const dbContent = fs.readFileSync(this.dbPath, {encoding: 'utf8'});
        return JSON.parse(dbContent);
    }

    getMovies(): Array<Movie> {
        return this.movies;
    }

    addMovie(movie: Movie): Movie {
        movie.id = ++this.lastIndex;
        this.movies.push(movie);
        this.saveFile();
        return movie;
    }
}
