export default interface Movie {
    id: number
    title: string;
    genres: Array<string>;
    year: number;
    runtime: number;
    director: string;
    actors: string;
    plot: string;
    posterUrl: string;
}

export const MovieGenres = [
    'Comedy',
    'Fantasy',
    'Crime',
    'Drama',
    'Music',
    'Adventure',
    'History',
    'Thriller',
    'Animation',
    'Family',
    'Mystery',
    'Biography',
    'Action',
    'Film-Noir',
    'Romance',
    'Sci-Fi',
    'War',
    'Western',
    'Horror',
    'Musical',
    'Sport'
];
