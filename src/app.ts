import * as express from 'express';
import Controller from './interfaces/controller.interface';
import errorMiddleware from './middlewares/error.middleware';

export default class App {
    public app: express.Application;
    public port: number | string = process.env.PORT || 3000;

    constructor(controllers: Controller[]) {
        this.app = express();

        this.initMiddlewares();
        this.initControllers(controllers);
        this.initErrorMiddleware();
    }

    private initControllers(controllers: Controller[]) {
        controllers.forEach((controller: Controller) => {
            this.app.use(`/api/${controller.path}`, controller.router)
        })
    }

    private initMiddlewares() {
        this.app.use(express.json());
    }

    private initErrorMiddleware() {
        this.app.use(errorMiddleware);
    }

    listen(): void {
        this.app.listen(this.port, () => {
            console.log('App is running on port', this.port)
        });
    }
}
