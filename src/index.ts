require('dotenv').config();
import 'reflect-metadata';

import App from './app';
import MovieModule from './modules/movie/movie.module';

const app = new App([MovieModule]);

app.listen();
